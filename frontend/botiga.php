<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Ortopedics</title>
</head>

<body>
    <?php
    //Check if user is logged in
    session_start();
    //check error
    $isLogged = true;
    if (!isset($_SESSION['usu_nom'])) {
        $error = "Has d'iniciar sessió.";
        $_SESSION['error'] = $error;
        $isLogged = false;
    }
    ?>
    <header>
        <section id="logo">
            <h1>ORTOPEDICS</h1>
            <p>La millor botiga per a tú</p>
        </section>
        <nav>
            <ul>
                <li><a href="./login.html">Iniciar sessió</a></li>
                <li><a href="./includes/logout.proc.php">Sortir</a></li>
                <?php

                //if user is logged in, show edit product menu
                if ($isLogged) {
                    echo "<li><a href='../backend/products/gestioProductes.php'>Gestionar productes</a></li>";
                }

                //if admin is logged in, show edit user menu too
                if ($_SESSION['usu_nivell'] == "admin") {
                    echo "<li><a href='../backend/users/gestioUsuaris.php'>Gestionar Usuaris</a></li>";
                }
                ?>

            </ul>

        </nav>
    </header>
    <main>
        <section id="productes">
            <h2>Botiga</h2>
            <?php
            include("../database/database.php");
            //TODO For each product loop, check if is_active before adding
            $qSELECTALLPRODUCTS = "SELECT * FROM producte WHERE pro_is_active = 1";
            $products = mysqli_query($conn, $qSELECTALLPRODUCTS);

            while ($row = mysqli_fetch_array($products)) {
                echo "
                <section class='producte'>
                <div class='product-image'>";
                if($row['pro_img'] == ""){
                    echo "<img src='../backend/products/imatgesProductes/placeholder.png' alt='imatge de producte'>";
                }else {
                    echo "<img src='../backend/products/imatgesProductes/$row[pro_img]' alt='imatge de producte'>";
                }
                    
                echo "</div>
                <div class='product-content'>
                    <h1>$row[pro_nom]</h1>
                    <p>$row[pro_preu]€</p>
                    <p>$row[pro_descripcio]</p>
                    <button>Comprar</button>
                </div>
                </section>
                ";
            }
            //TODO section class producte with good format inside
            
            ?>

        </section>
    </main>
    <footer>
        <section class="logo">
            <h1>ORTOPEDICS</h1>
            <p>La millor botiga per a tú</p>
        </section>
        <section class="dades">
            <p><span>Telèfon</span>123456789</p>
            <p><span>Email</span>Ortopedics@gmail.com</p>
            <p><span>Direcció</span>Calle falsa 123</p>
        </section>
    </footer>



</body>
</html>