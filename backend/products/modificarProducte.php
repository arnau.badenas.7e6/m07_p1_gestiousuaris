<?php
include("../../database/database.php");
$pro_id = $_REQUEST['old_pro_id'];
$resul = mysqli_query($conn, "SELECT * FROM producte WHERE pro_id = $pro_id");
while ($res = mysqli_fetch_array($resul)) {
    $pro_nom = $res['pro_nom'];
    $pro_preu = $res['pro_preu'];
    $pro_imatge = $res['pro_imatge'];
    $pro_descripcio = $res['pro_descripcio'];
}
?>
<form action="modificarProducte.proc.php" method="POST" enctype="multipart/form-data">
    <table>
        <input type="hidden" name="pro_id" value="<?php echo $pro_id; ?>">
        <tr>
            <td>Nom producte:</td>
            <td><input name="pro_nom" size="20" value=<?php echo $pro_nom; ?>></td>
        </tr>
        <tr>
            <td>Preu producte:</td>
            <td><input type="number" name="pro_preu" value=<?php echo $pro_preu; ?> step="0.01">€</td>
        </tr>
        <tr>
            <td>Imatge:</td>
            <td><input type="file" name="pro_img" value=<?php echo $pro_imatge; ?>></td>
        </tr>
        <tr>
            <td>Descripció</td>
            <td>
                <textarea rows="5" cols="40" name="pro_descripcio" id="pro_descripcio"
                    spellcheck="false"><?php echo $pro_descripcio; ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Enviar"></td>
        </tr>
    </table>
</form>