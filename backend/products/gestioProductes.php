<!-- TOT OK -->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Gestió d'usuaris</title>
</head>

<body>
<?php
//Check if user is logged in
    session_start();
    //check error
    if (!isset($_SESSION['usu_nom'])) {
        $error = "Has d'iniciar sessió.";
        $_SESSION['error'] = $error;
        header("Location: ../users/error.php");
        exit;
    }
?>
    <nav class="d-flex justify-content-between">
        <h1>Benvingut
            <?php echo $_SESSION['usu_nom']; ?>
        </h1>
        <div class="list-group">
            <a href="../../frontend/includes/logout.proc.php" class="list-group-item list-group-item-action">Sortir</a>
            <a href="../../frontend/botiga.php" class="list-group-item list-group-item-action">Tornar</a>
        </div>
    </nav>

    <table class="table caption-top">
        <caption>Taulell de gestió de productes</caption>
        <tr class="table-dark">
            <th>Producte</th>
            <th>Descripció</th>
            <th>Preu</th>
            <th>Imatge</th>
            <th>Activat</th>
            <th>Operacions</th>
        </tr>
        <!-- Taula per gestionar usuaris: modificar eliminar i borrar -->
        <?php
        include("../../database/database.php");
        //query
        $resul = mysqli_query($conn, "SELECT * FROM producte ORDER BY pro_id");
        //Agafar tots els usuaris agafats i mostrar-los en una taula
        while ($res = mysqli_fetch_array($resul)) {
        $activat = ($res['pro_is_active'] == 1) ? "Si" : "No";
        echo "<tr>
        <td>$res[pro_nom]</td>
        <td>$res[pro_descripcio]</td>
        <td>$res[pro_preu]€</td>
        <td>$res[pro_img]</td>
        <td>$activat</td>
        <td><a href='./modificarProducte.php?old_pro_id=$res[pro_id]'>Modificar</a> <a href='./activarDesactivarProducte.proc.php?pro_id=$res[pro_id]'>Activar/Desactivar</a></td>
        </tr>";
        }
        mysqli_close($conn);
        ?>
        <tfoot>
            <tr>
                <td><a href='crearProducte.php' class="btn btn-dark" role="button">+ Afegir Producte</a></td>
            </tr>
        </tfoot>
    </table>


</body>

</html>