<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Modificar Usuari</title>
</head>

<body>
    <?php
    include("./includes/checkSession.php");
    ?>
    <?php
    $old_usu_nom = $_REQUEST['old_usu_nom'];
    ?>

    <form action="./includes/modificarUsuari.proc.php" method="POST" class="w-50 mx-auto">
        <h1>Modificar usuari
            <?php echo "$old_usu_nom"; ?>
        </h1>
        <input type="hidden" name="old_usu_nom" value="<?php echo "$old_usu_nom"; ?>">
        <fieldset class="form-group">
            <label for="usu_nom">Nou nom d'usuari</label>
            <input type="text" class="form-control" name="usu_nom" size="20">
        </fieldset>
        <fieldset class="form-group">
            <label for="usu_password">Nova contrasenya</label>
            <input type="password" class="form-control" name="usu_password" size="20">
        </fieldset>
        <?php
        if ($_SESSION['usu_nom'] != $old_usu_nom) {
            echo "<fieldset class='form-group'>
            <label for='usu_nivell'>Nou nivell d'accés</label>
            <select id='usu_nivell' class='form-select' name='usu_nivell' size='1'>
                <option value='user'>User</option>
                <option value='admin'>Admin</option>
            </select>
            </fieldset>";
        }

        ?>

        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</body>

</html>