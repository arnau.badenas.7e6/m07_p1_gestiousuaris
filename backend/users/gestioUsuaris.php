<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Gestió d'usuaris</title>
</head>

<body>
    <?php include("./includes/checkSession.php"); ?>
    <nav class="d-flex justify-content-around">
        <h1>Benvingut
            <?php echo $_SESSION['usu_nom']; ?>
        </h1>
        <div class="list-group">
            <a href="../../frontend/includes/logout.proc.php" class="list-group-item list-group-item-action">Sortir</a>
            <a href="../../frontend/botiga.php" class="list-group-item list-group-item-action">Tornar</a>

        </div>
    </nav>

    <table class="table caption-top">
        <caption>Taulell de gestió d'usuaris</caption>
        <tr class="table-dark">
            <th>Nom d'Usuari</th>
            <th>Nivell d'accés</th>
            <th>Operacions</th>
        </tr>
        <!-- Taula per gestionar usuaris: modificar eliminar i borrar -->
        <?php
        include("../../database/database.php");
        //query
        $resul = mysqli_query($conn, "SELECT * FROM usuari ORDER BY usu_nivell");
        //Agafar tots els usuaris agafats i mostrar-los en una taula
        while ($res = mysqli_fetch_array($resul)) {
            echo "<tr>
        <td>$res[usu_nom]</td>
        <td>$res[usu_nivell]</td>";
            if ($res['usu_nivell'] == "user") {
                echo "<td><a href='./modificarUsuari.php?old_usu_nom=$res[usu_nom]'>Modificar</a> <a href='./includes/eliminarUsuari.proc.php?usu_nom=$res[usu_nom]'>Eliminar</a> </td>
            </tr>";
            } else if ($res['usu_nom'] == $_SESSION['usu_nom']) {
                echo "<td><a href='./modificarUsuari.php?old_usu_nom=$res[usu_nom]'>Modificar</a></td>";
            }else{
                echo "<td></td>";
            }
        }
        mysqli_close($conn);
        ?>
        <tfoot>
            <tr>
                <td><a href='crearUsuari.php' class="btn btn-dark" role="button">+ Afegir usuari</a></td>
            </tr>
        </tfoot>
    </table>


</body>

</html>