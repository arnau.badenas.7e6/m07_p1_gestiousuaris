<?php
include("../../../database/database.php");
//Agafar els atributs del usuari actuals
$old_usu_nom = $_REQUEST['old_usu_nom'];

//Agafar els atributs nous desitjats
$usu_nom = $_REQUEST['usu_nom'];
$usu_password = md5($_REQUEST['usu_password']);
$usu_nivell = $_REQUEST['usu_nivell'];

//Comprovar si el nom existeix
$checkUsernameQuery = "SELECT usu_nom FROM usuari WHERE usu_nom LIKE '$usu_nom'";
$checkUsername = mysqli_query($conn, $checkUsernameQuery);
if (mysqli_num_rows($checkUsername) > 0) { // Si aquest nom d'usuari ja existeix
    header("location: ../error.php?error='L'usuari no s''ha pogut modificar perquè aquest nom d'usuari ja existeix'");
} else { //TOT OK
    $usuUpdate = "UPDATE usuari SET usu_nom ='$usu_nom' ";

    //Si la contrasenya no esta posada, no canvia
    if (isset($usu_password)) {
        $usuUpdate .= ",usu_password='$usu_password' ";
    }
    //Si es modifica ell mateix, no pot canviar el nivell
    if ($_SESSION['usu_nom'] != $old_usu_nom) {
        $usuUpdate .= ",usu_nivell='$usu_nivell' ";
    }
    $usuUpdate .= "WHERE usu_nom = '$old_usu_nom'";
    
    if (mysqli_query($conn, $usuUpdate)) {
        header("location: ../gestioUsuaris.php?message='Usuari modificat correctament'");
    } else {
        header("location: ../error.php?error='ERROR DESCONEGUT: L'usuari no s''ha pogut modificar'");
    } 
}
mysqli_close($conn);
?>