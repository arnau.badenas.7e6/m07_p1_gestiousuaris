<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Error</title>
</head>

<body class="bg-light d-flex flex-column justify-content-center align-items-center vh-100">
    <div class="text-center">
        <h1 class="display-4">ERROR</h1>
        <p class="lead mb-5">Ho sentim, hi ha hagut algun problema:</p>
        <div id="Error" class="alert alert-danger mb-5" role="alert">
            <?php
            session_start();
            if(isset($_SESSION['error'])){
                $error = $_SESSION['error'];
            }else{
                $error = $_REQUEST['error'];
            }
            echo "$error";
            session_destroy();
            
            ?>
        </div>
        <a href="../../frontend/login.html" class="btn btn-primary">Tornar</a>
    </div>
</body>

</html>