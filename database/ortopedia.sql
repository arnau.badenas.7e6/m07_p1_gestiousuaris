-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Temps de generació: 22-04-2023 a les 13:07:47
-- Versió del servidor: 8.0.32-0ubuntu0.22.04.2
-- Versió de PHP: 8.1.2-1ubuntu2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `ortopedia`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `producte`
--

CREATE TABLE `producte` (
  `pro_id` int NOT NULL COMMENT 'id de producte auto incremental',
  `pro_preu` decimal(20,2) NOT NULL,
  `pro_nom` varchar(255) NOT NULL,
  `pro_descripcio` text,
  `pro_img` varchar(255) DEFAULT NULL COMMENT 'nom de la imatge del producte',
  `pro_is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 es true i 0 es false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Bolcament de dades per a la taula `producte`
--

INSERT INTO `producte` (`pro_id`, `pro_preu`, `pro_nom`, `pro_descripcio`, `pro_img`, `pro_is_active`) VALUES
(5, '10.00', 'asd', 'asd', '5.png', 1),
(6, '10.00', 'test2', 'asdasd', '6.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de la taula `usuari`
--

CREATE TABLE `usuari` (
  `usu_nom` varchar(50) NOT NULL,
  `usu_password` varchar(200) NOT NULL,
  `usu_nivell` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Bolcament de dades per a la taula `usuari`
--

INSERT INTO `usuari` (`usu_nom`, `usu_password`, `usu_nivell`) VALUES
('admin', '200820e3227815ed1756a6b531e7e0d2', 'admin'),
('user', '200820e3227815ed1756a6b531e7e0d2', 'user'),
('admin2', '200820e3227815ed1756a6b531e7e0d2', 'admin');

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `producte`
--
ALTER TABLE `producte`
  ADD PRIMARY KEY (`pro_id`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `producte`
--
ALTER TABLE `producte`
  MODIFY `pro_id` int NOT NULL AUTO_INCREMENT COMMENT 'id de producte auto incremental', AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
